import React, {useState} from "react";
import '../styles/login.css'
import {useNavigate} from "react-router-dom";

const Login = ({setLoggedIn,username, setUsername,password, setPassword}) => {

    let history = useNavigate();

    function handleSubmit(e){
        e.preventDefault();
        if(username === "admin" && password==="admin") {
            setLoggedIn(true);
        }
        history("/home");
    }
    return (
        <div className="form-control">
            <form>
                <div className="form">
                    <h1>Login Page</h1>
                    <label className="user-name-text">User</label>
                    <input type="text"
                           placeholder="Username"
                           value={username}
                           className="user-name"
                           onChange={(e) => {
                               setUsername(e.target.value)
                           }}/>
                    <label className="user-pass-text">Password</label>
                    <input type="password"
                           placeholder="Password"
                           className="user-pass"
                           value={password}
                           onChange={(e) => {
                               setPassword(e.target.value)
                           }}/>
                    <button type="submit"
                            className="send-form"
                            onClick={handleSubmit}>Submit
                    </button>
                </div>
            </form>
        </div>
    );
}
export default Login;