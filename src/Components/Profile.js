import React from "react";
import '../styles/profile.css'

const Profile = () => {
    return (
        <div className="profile">
            <h1>Profile page</h1>
            <h2>Admin Adminovich</h2>
            <h3>30 years old...</h3>
        </div>
    );
}
export default Profile;