import '../styles/header.css'
import React, {useState} from "react";
import {Link} from "react-router-dom";

const Header = ({loggedIn,setPassword,setUsername}) => {
    return (
        <React.Fragment>
            <div className="header">
                <div className="header-optioins">
                    <div className="header-first-options">
                        <Link to="/home" className="header-option header-home">Home</Link>

                        {loggedIn ? <Link to="/profile" className="header-option header-profile">Profile</Link> :
                            <Link to="/login" className="header-option header-profile">Profile</Link>}

                        {loggedIn ? <Link to="/friends" className="header-option header-friend">Friends</Link> :
                            <Link to="/login" className="header-option header-friend">Friends</Link>}

                    </div>

                    {loggedIn ? <Link to="/login" className="header-option header-enter"
                        onClick={()=>{setPassword(''); setUsername('')}}>Logout</Link> :
                        <Link to="/login" className="header-option header-enter">Login</Link>}

                </div>
            </div>
        </React.Fragment>
    );
}
export default Header;