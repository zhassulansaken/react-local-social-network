import React from "react";
import '../styles/friend.css'
import {Link} from "react-router-dom";

const Alex = () => {
    return (
        <div className="friends">
            <div className="profile">
                <h1>Friends page</h1>
                <Link to="/friends/Alex" className="friend-link">Alex</Link>
                <Link to="/friends/John" className="friend-link">John</Link>
            </div>
            <div className="profile">
                <h1>Profile page</h1>
                <h2>Alex Alexeev</h2>
                <h3>25 years old...</h3>
            </div>
        </div>
    );
}
export default Alex;
