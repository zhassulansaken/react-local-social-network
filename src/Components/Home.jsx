import React from "react";
import '../styles/home.css'


const Home = ({loggedIn}) => {
    return (
        <div>
            <h1 className="welcome-text">
                Welcome to the homepage!
            </h1>
            { loggedIn ? <h2 className="not-auth-text">You have successfully authed in your account!
                    You can now enter your pages</h2>:
                <h2 className="not-auth-text">You are not authed! Please login to the System!</h2>}
        </div>
    );
}
export default Home;