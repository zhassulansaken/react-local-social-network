import React from "react";
import '../styles/friend.css'
import {Link} from "react-router-dom";

const Friend = () => {
    return (
        <div className="profile">
            <h1>Friends page</h1>
            <Link to="/friends/Alex" className="friend-link">Alex</Link>
            <Link to="/friends/John" className="friend-link">John</Link>
        </div>
    );
}
export default Friend;
