import React, {useState} from 'react';
import Header from "./Components/Header";
import Home from "./Components/Home";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import './styles/login.css'
import Login from "./Components/Login";
import Profile from "./Components/Profile";
import Friend from "./Components/Friend";
import Alex from "./Components/Alex";
import John from "./Components/John";

function App() {
    const [loggedIn, setLoggedIn] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    return (
        <Router>
            <Header loggedIn={loggedIn} setUsername={setUsername} setPassword={setPassword}/>
            <Routes>
                { loggedIn ===false ?
                    <Route path="/home" element={<Home loggedIn={loggedIn}/>}/> :
                    <Route path="/home" element={<Home loggedIn={loggedIn}/>}/>
                }

                <Route path="/profile" element={<Profile/>}/>
                <Route path="/friends" element={<Friend/>}/>

                <Route path="/friends/Alex" element={<Alex/>}/>

                <Route path="/friends/John" element={<John/>}/>

                <Route path="/login" element={<Login setLoggedIn={setLoggedIn} setPassword={setPassword}
                                     username={username} setUsername={setUsername} password={password}/>}/>

            </Routes>
        </Router>
    );
}

export default App;


